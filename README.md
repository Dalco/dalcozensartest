This creates a webservice reading from a pre-configured Apigee endpoint.

Build with:

mvn clean package spring-boot:repackage

Run with:

java -jar target/gs-spring-boot-*.jar

Query from another shell with:

curl http://localhost:8080/reducedPriceProducts