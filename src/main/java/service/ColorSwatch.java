package service;

import java.util.Objects;

class ColorSwatch {
    public String color;
    public String rgbColor;
    public String skuid;

    public ColorSwatch() {}

    public ColorSwatch(String color, String rgbColor, String skuid) {
        this.color = color;
        this.rgbColor = rgbColor;
        this.skuid = skuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ColorSwatch that = (ColorSwatch) o;
        return color.equals(that.color) &&
                rgbColor.equals(that.rgbColor) &&
                skuid.equals(that.skuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, rgbColor, skuid);
    }

    @Override
    public String toString() {
        return "ApigeeColorSwatch{" +
                "color='" + color + '\'' +
                ", rgbColor='" + rgbColor + '\'' +
                ", skuid='" + skuid + '\'' +
                '}';
    }
}
