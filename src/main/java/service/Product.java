package service;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

class Product {
    public String productId;
    public String title;
    public List<ColorSwatch> colorSwatches;
    public String nowPrice;
    public String priceLabel;
    @JsonIgnore
    public BigDecimal reduction;

    @Override
    public String toString() {
        return "Product{" +
                "productId='" + productId + '\'' +
                ", title='" + title + '\'' +
                ", colorSwatches=" + colorSwatches +
                ", nowPrice='" + nowPrice + '\'' +
                ", priceLabel='" + priceLabel + '\'' +
                ", reduction=" + reduction +
                '}';
    }

    public Product() {}

    public Product(String productId, String title, List<ColorSwatch> colorSwatches, String nowPrice, String priceLabel, BigDecimal reduction) {
        this.productId = productId;
        this.title = title;
        this.colorSwatches = colorSwatches;
        this.nowPrice = nowPrice;
        this.priceLabel = priceLabel;
        this.reduction = reduction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return productId.equals(product.productId) &&
                Objects.equals(title, product.title) &&
                Objects.equals(colorSwatches, product.colorSwatches) &&
                Objects.equals(nowPrice, product.nowPrice) &&
                Objects.equals(priceLabel, product.priceLabel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, title, colorSwatches, nowPrice, priceLabel);
    }
}
