package service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import service.fromapigee.ApigeeColorSwatch;
import service.fromapigee.ApigeePrice;
import service.fromapigee.ApigeeProduct;
import service.fromapigee.ApigeeProductList;
import service.utils.colours.ColorUtils;
import service.utils.currency.CurrencyUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
class ReducedPriceProductsController {
    @Value("${apigeeURL}")
    private String apigeeURL;

    @RequestMapping("/reducedPriceProducts")
    public List<Product> reducedPriceProducts(@RequestParam(required = false, defaultValue = "ShowWasNow") PriceLabelType priceLabelType) {
        RestTemplate template = new RestTemplate();
        ApigeeProductList response = template.getForObject(apigeeURL, ApigeeProductList.class);
        List<ApigeeProduct> apigeeProducts = response.products;

        System.out.println("Found " + apigeeProducts.size() + " products");

        List<Product> products = apigeeProducts.isEmpty() ? Collections.emptyList() :
                apigeeProducts
                        .stream()
                        // When 'now' is not a String or empty, the parsing rule is not defined, so I can't consider it reduced, and therefore I exclude it
                        .filter(p -> !("".equals(p.price.now)) && (null != p.price.now))
                        .filter(p -> !("".equals(p.price.was)) && (null != p.price.was))
                        .map(p -> convert(p, priceLabelType))
                        .sorted((p1, p2) -> p2.reduction.compareTo(p1.reduction))
                        .collect(Collectors.toList());

        System.out.println("Returning " + products.size() + " products");

        return products;
    }

    private Product convert(ApigeeProduct apigeeProduct, PriceLabelType priceLabelType) {
        return new Product(
                apigeeProduct.productId,
                apigeeProduct.title,
                apigeeProduct.colorSwatches.stream().map(ReducedPriceProductsController::convert).collect(Collectors.toList()),
                getDisplayPrice(apigeeProduct.price),
                generatePriceLabel(apigeeProduct.price, priceLabelType),
                calculateReduction(apigeeProduct.price));
    }

    private BigDecimal calculateReduction(ApigeePrice price) {
        return new BigDecimal(price.was).subtract(new BigDecimal(price.now));
    }

    private static ColorSwatch convert(ApigeeColorSwatch apigeeColorSwatch) {
        return new ColorSwatch(apigeeColorSwatch.color, ColorUtils.getColourRGB(apigeeColorSwatch.color), apigeeColorSwatch.skuid);
    }

    private String generatePriceLabel(ApigeePrice apigeePrice, PriceLabelType priceLabelType) {
        switch(priceLabelType) {
            case ShowPercDiscount:
                return getPercOff(apigeePrice) + "% off - now " + getDisplayPrice(apigeePrice);
            case ShowWasThenNow:
                String thenPrice = apigeePrice.then2 != null? apigeePrice.then2 : apigeePrice.then1;
                return getWasThenNowLabelPrice(apigeePrice.was, thenPrice, apigeePrice.now, apigeePrice.currency);
            default:
                return getWasThenNowLabelPrice(apigeePrice.was, null, apigeePrice.now, apigeePrice.currency);
        }
    }

    private final static DecimalFormat percentageFormat = new DecimalFormat("#0.#");
    private String getPercOff(ApigeePrice apigeePrice) {
        BigDecimal nowPrice = new BigDecimal(apigeePrice.now);
        BigDecimal wasPrice = new BigDecimal(apigeePrice.was);

        BigDecimal percOff = BigDecimal.ONE.subtract(nowPrice.divide(wasPrice)).multiply(BigDecimal.valueOf(100L));
        return percentageFormat.format(percOff.doubleValue());
    }

    private String getWasThenNowLabelPrice(String was, String then, String now, String currency) {
        return "Was " + getDisplayPrice(was, currency) + (then == null ? "" : ", then " + getDisplayPrice(then, currency)) + ", now " + getDisplayPrice(now, currency);
    }

    private String getDisplayPrice(String price, String currencyCode) {
        return CurrencyUtils.formatPriceOrEmpty(currencyCode, price);
    }

    private String getDisplayPrice(ApigeePrice apigeePrice) {
        return CurrencyUtils.formatPriceOrEmpty(apigeePrice.currency, apigeePrice.now);
    }
}

enum PriceLabelType {
    ShowWasNow,
    ShowWasThenNow,
    ShowPercDiscount
}