package service.fromapigee;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApigeeColorSwatch {
    public String color;
    public String skuid;

    public ApigeeColorSwatch() {}

    public ApigeeColorSwatch(String color, String skuid) {
        this.color = color;
        this.skuid = skuid;
    }
}
