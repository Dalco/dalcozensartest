package service.fromapigee;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApigeePrice {
    public String was;
    public String then1;
    public String then2;
    public String now;
    public String currency;

    public void setNow(Object now) { if (now instanceof String) this.now = (String)now;}

    public ApigeePrice() {}

    public ApigeePrice(String was, String then1, String then2, String now, String currency) {
        this.was = was;
        this.then1 = then1;
        this.then2 = then2;
        this.now = now;
        this.currency = currency;
    }
}
