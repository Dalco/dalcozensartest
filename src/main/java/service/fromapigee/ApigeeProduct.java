package service.fromapigee;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApigeeProduct {
    public String productId;
    public String title;
    public List<ApigeeColorSwatch> colorSwatches;
    public ApigeePrice price;

    public ApigeeProduct() {}

    public ApigeeProduct(String productId, String title, List<ApigeeColorSwatch> colorSwatches, ApigeePrice price) {
        this.productId = productId;
        this.title = title;
        this.colorSwatches = colorSwatches;
        this.price = price;
    }
}
