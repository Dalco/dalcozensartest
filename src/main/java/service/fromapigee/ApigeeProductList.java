package service.fromapigee;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApigeeProductList {
    public List<ApigeeProduct> products;

    public ApigeeProductList() {}

    public ApigeeProductList(List<ApigeeProduct> products) {
        this.products = products;
    }
}
