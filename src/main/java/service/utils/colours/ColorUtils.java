package service.utils.colours;

import java.util.HashMap;
import java.util.Map;

public class ColorUtils {
    private static final Map<String, String> colourRGBsByName = new HashMap<String, String>() {{
        put("Mimosa Yellow", "123456");
        put("Navy", "F0A1C2");
        put("White", "FFFFFF");
    }};

    public static String getColourRGB(String colour) {
        return colourRGBsByName.getOrDefault(colour, "");
    }
}
