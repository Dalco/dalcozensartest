package service.utils.currency;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

public class CurrencyUtils {
    private static final Map<String, String> currencySymbolsByCode = new HashMap<String, String>() {{
        put("GBP", "£");
        put("EUR", "€");
        put("USD", "$");
    }};

    public static String formatPriceOrEmpty(String currencyCode, String price) {
        String trimmedPrice = price.trim();
        if (trimmedPrice.equals(""))
            return "";

        return formatPrice(currencyCode, trimmedPrice);
    }

    private static String formatPrice(String currencyCode, String price) {
        String currencySymbol = currencySymbolsByCode.getOrDefault(currencyCode, "");
        BigDecimal priceNumber = new BigDecimal(price);
        NumberFormat integerFormat = new DecimalFormat("#0");
        String maybeIntegerPrice = priceNumber.compareTo(BigDecimal.TEN) >= 0
                && priceNumber.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO) == 0
                ? integerFormat.format(priceNumber)
                : price;
        return currencySymbol + maybeIntegerPrice; // Not coping with locales. I consider it out of the scope of this test.
    }
}
