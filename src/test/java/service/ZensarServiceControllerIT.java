package service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;
import service.fromapigee.ApigeeColorSwatch;
import service.fromapigee.ApigeePrice;
import service.fromapigee.ApigeeProduct;
import service.fromapigee.ApigeeProductList;

import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "apigeeURL=http://localhost:8089/reducedPriceProducts", webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ZensarServiceControllerIT {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8089);

    @LocalServerPort
    private int port;

    private URL base;

    @Autowired
    private TestRestTemplate template;

    @Before
    public void setUp() throws Exception {
        this.base = new URL("http://localhost:" + port + "/reducedPriceProducts");
    }

    @Test
    public void endpointListens() {
        // GIVEN
        String apigeeResponse = getApigeeResponse("");

        // WHEN
        stubFor(get(urlEqualTo("/reducedPriceProducts")).withHeader("Accept", containing("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apigeeResponse)));

        ResponseEntity<String> response = template.getForEntity(base.toString(), String.class);
        assertThat(response.getStatusCodeValue(), Matchers.equalTo(200));
    }

    @Test
    public void endpointShouldReturnAnArrayOfProducts() {
        // GIVEN
        String apigeeResponse = getApigeeResponse(getApigeeProductJSON("\"\"", "\"\"", "\"101\"", "\"100\"", "\"\""));

        // WHEN
        stubFor(get(urlEqualTo("/reducedPriceProducts")).withHeader("Accept", containing("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apigeeResponse)));

        ProductList result = template.getForEntity(base.toString(), ProductList.class).getBody();

        assertNotNull(result);
        assertTrue(result.size() > 0);
        assertNotNull(result.get(0));
    }

    @Test
    public void endpointShouldReturnASpecificProduct() {
        // GIVEN
        String apigeeResponse = getApigeeResponse(getApigeeProductJSON("\"pid\"", "\"title\"", "\"2.00\"", "\"1.00\"", "\"GBP\""));

        // WHEN
        stubFor(get(urlEqualTo("/reducedPriceProducts")).withHeader("Accept", containing("application/json"))
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody(apigeeResponse)));

        ResponseEntity<ProductList> response = template.getForEntity(base.toString(),
                ProductList.class);

        // THEN
        ProductList result = response.getBody();

        Product product = new Product("pid", "title", Collections.emptyList(), "£1.00", "Was £2.00, now £1.00", BigDecimal.ONE);
        assertEquals(1, result.size());
        Assert.assertEquals(product, result.get(0));
    }

    @Test
    public void returnedProductsShouldHaveColorSwatchesWithColourNames() throws JsonProcessingException {
        // GIVEN
        ApigeePrice apigeePrice = new ApigeePrice("2.00", null, null, "1.00", "GBP");
        ApigeeColorSwatch apigeeColorSwatch1 = new ApigeeColorSwatch("Navy", "skuid1");
        ApigeeColorSwatch apigeeColorSwatch2 = new ApigeeColorSwatch("Mimosa Yellow", "skuid2");
        List<ApigeeColorSwatch> apigeeColorSwatches = Arrays.asList(apigeeColorSwatch1, apigeeColorSwatch2);
        ApigeeProduct apigeeProduct = new ApigeeProduct("pid1", "title1", apigeeColorSwatches, apigeePrice);
        String apigeeResponse = json(new ApigeeProductList(Arrays.asList(apigeeProduct)));

        // WHEN
        stubFor(get(urlEqualTo("/reducedPriceProducts")).withHeader("Accept", containing("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apigeeResponse)));

        ResponseEntity<ProductList> response = template.getForEntity(base.toString(), ProductList.class);

        // THEN
        ProductList result = response.getBody();

        assertEquals(1, result.size());
        List<ColorSwatch> expectedColorSwatches = Arrays.asList(new ColorSwatch("Navy", "F0A1C2", "skuid1"),
                new ColorSwatch("Mimosa Yellow", "123456", "skuid2"));
        Assert.assertEquals(expectedColorSwatches, result.get(0).colorSwatches);
    }

    private String json(ApigeeProductList apigeeProductList) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(apigeeProductList);
    }

    @Test
    public void returnedProductsPricesShouldBeFormattedWithCurrency() throws JsonProcessingException {
        // GIVEN
        ApigeePrice apigeePrice1 = new ApigeePrice("2.00", null,null, "1.00", "GBP");
        ApigeePrice apigeePrice2 = new ApigeePrice("2.00", null, null,"1.00", "USD");
        ApigeeProduct apigeeApigeeProduct1 = new ApigeeProduct("pid1", "title1", Collections.emptyList(), apigeePrice1);
        ApigeeProduct apigeeApigeeProduct2 = new ApigeeProduct("pid2", "title2", Collections.emptyList(), apigeePrice2);
        String apigeeResponse = json(new ApigeeProductList(Arrays.asList(apigeeApigeeProduct1, apigeeApigeeProduct2)));

        // WHEN
        stubFor(get(urlEqualTo("/reducedPriceProducts")).withHeader("Accept", containing("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apigeeResponse)));

        ResponseEntity<ProductList> response = template.getForEntity(base.toString(), ProductList.class);

        // THEN
        ProductList result = response.getBody();

        assertEquals(2, result.size());
        Assert.assertEquals("£1.00", result.get(0).nowPrice);
        Assert.assertEquals("$1.00", result.get(1).nowPrice);
    }

    @Test
    public void returnedProductsPricesShouldShowNoDecimalsWhenTenPoundsOrHigherAndInteger() throws JsonProcessingException {
        // GIVEN
        ApigeePrice apigeePrice1 = new ApigeePrice("102.00", null, null,"9.99", "GBP");
        ApigeePrice apigeePrice2 = new ApigeePrice("102.00", null, null,"10.00", "GBP");
        ApigeePrice apigeePrice3 = new ApigeePrice("102.00", null, null,"10.55", "GBP");
        ApigeePrice apigeePrice4 = new ApigeePrice("102.00", null, null,"100.00", "GBP");
        ApigeeProduct apigeeApigeeProduct1 = new ApigeeProduct("pid1", "title1", Collections.emptyList(), apigeePrice1);
        ApigeeProduct apigeeApigeeProduct2 = new ApigeeProduct("pid2", "title2", Collections.emptyList(), apigeePrice2);
        ApigeeProduct apigeeApigeeProduct3 = new ApigeeProduct("pid3", "title3", Collections.emptyList(), apigeePrice3);
        ApigeeProduct apigeeApigeeProduct4 = new ApigeeProduct("pid4", "title4", Collections.emptyList(), apigeePrice4);
        List<ApigeeProduct> apigeeProducts = Arrays.asList(apigeeApigeeProduct1, apigeeApigeeProduct2, apigeeApigeeProduct3, apigeeApigeeProduct4);
        String apigeeResponse = json(new ApigeeProductList(apigeeProducts));

        // WHEN
        stubFor(get(urlEqualTo("/reducedPriceProducts")).withHeader("Accept", containing("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apigeeResponse)));

        ResponseEntity<ProductList> response = template.getForEntity(base.toString(), ProductList.class);

        // THEN
        ProductList result = response.getBody();

        assertEquals(result.size(), apigeeProducts.size());
        Assert.assertEquals("£9.99",   result.get(0).nowPrice);
        Assert.assertEquals(  "£10",   result.get(1).nowPrice);
        Assert.assertEquals( "£10.55", result.get(2).nowPrice);
        Assert.assertEquals( "£100",   result.get(3).nowPrice);
    }

    @Test
    public void showWasNowPriceLabel() throws JsonProcessingException {
        // GIVEN
        ApigeePrice apigeePrice = new ApigeePrice("12.99", null, null,"9.99", "GBP");
        ApigeeProduct apigeeApigeeProduct = new ApigeeProduct("pid1", "title1", Collections.emptyList(), apigeePrice);
        String apigeeResponse = json(new ApigeeProductList(Collections.singletonList(apigeeApigeeProduct)));

        // WHEN
        stubFor(get(urlEqualTo("/reducedPriceProducts")).withHeader("Accept", containing("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apigeeResponse)));

        String requestUrl = UriComponentsBuilder
                .fromHttpUrl(base.toString())
                .queryParam("priceLabelType", "ShowWasNow")
                .toUriString();
        ResponseEntity<ProductList> response = template.getForEntity(requestUrl, ProductList.class);

        // THEN
        ProductList result = response.getBody();

        assertEquals(1, result.size());
        Assert.assertEquals("Was £12.99, now £9.99", result.get(0).priceLabel);
    }

    @Test
    public void showWasNowPriceLabelWithTenPoundsOrMoreIntegers() throws JsonProcessingException {
        // GIVEN
        ApigeePrice apigeePrice = new ApigeePrice("22.00", null, null,"19.00", "GBP");
        ApigeeProduct apigeeApigeeProduct = new ApigeeProduct("pid1", "title1", Collections.emptyList(), apigeePrice);
        String apigeeResponse = json(new ApigeeProductList(Collections.singletonList(apigeeApigeeProduct)));

        // WHEN
        stubFor(get(urlEqualTo("/reducedPriceProducts")).withHeader("Accept", containing("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apigeeResponse)));

        String requestUrl = UriComponentsBuilder
                .fromHttpUrl(base.toString())
                .queryParam("priceLabelType", "ShowWasNow")
                .toUriString();
        ResponseEntity<ProductList> response = template.getForEntity(requestUrl, ProductList.class);

        // THEN
        ProductList result = response.getBody();

        assertEquals(1, result.size());
        Assert.assertEquals("Was £22, now £19", result.get(0).priceLabel);
    }

    @Test
    public void showWasThenNowPriceLabelWithThen2Price() throws JsonProcessingException {
        // GIVEN
        ApigeePrice apigeePrice = new ApigeePrice("12.99", "11.99", "10.99", "9.99", "GBP");
        ApigeeProduct apigeeApigeeProduct = new ApigeeProduct("pid1", "title1", Collections.emptyList(), apigeePrice);
        String apigeeResponse = json(new ApigeeProductList(Collections.singletonList(apigeeApigeeProduct)));

        // WHEN
        stubFor(get(urlEqualTo("/reducedPriceProducts"))
                .withHeader("Accept", containing("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apigeeResponse)));

        String requestUrl = UriComponentsBuilder
                .fromHttpUrl(base.toString())
                .queryParam("priceLabelType", "ShowWasThenNow")
                .toUriString();
        ResponseEntity<ProductList> response = template.getForEntity(requestUrl, ProductList.class);

        // THEN
        ProductList result = response.getBody();

        assertEquals(1, result.size());
        Assert.assertEquals("Was £12.99, then £10.99, now £9.99", result.get(0).priceLabel);
    }

    @Test
    public void showWasThenNowPriceLabelWithThen1PriceButNoThen2Price() throws JsonProcessingException {
        // GIVEN
        ApigeePrice apigeePrice = new ApigeePrice("12.99", "11.99", null, "9.99", "GBP");
        ApigeeProduct apigeeApigeeProduct = new ApigeeProduct("pid1", "title1", Collections.emptyList(), apigeePrice);
        String apigeeResponse = json(new ApigeeProductList(Collections.singletonList(apigeeApigeeProduct)));

        // WHEN
        stubFor(get(urlEqualTo("/reducedPriceProducts"))
                .withHeader("Accept", containing("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apigeeResponse)));

        String requestUrl = UriComponentsBuilder
                .fromHttpUrl(base.toString())
                .queryParam("priceLabelType", "ShowWasThenNow")
                .toUriString();
        ResponseEntity<ProductList> response = template.getForEntity(requestUrl, ProductList.class);

        // THEN
        ProductList result = response.getBody();

        assertEquals(1, result.size());
        Assert.assertEquals("Was £12.99, then £11.99, now £9.99", result.get(0).priceLabel);
    }

    @Test
    public void showWasThenNowPriceLabelWithNoThen1AndThen2Prices() throws JsonProcessingException {
        // GIVEN
        ApigeePrice apigeePrice = new ApigeePrice("12.99", null, null, "9.99", "GBP");
        ApigeeProduct apigeeApigeeProduct = new ApigeeProduct("pid1", "title1", Collections.emptyList(), apigeePrice);
        String apigeeResponse = json(new ApigeeProductList(Collections.singletonList(apigeeApigeeProduct)));

        // WHEN
        stubFor(get(urlEqualTo("/reducedPriceProducts"))
                .withHeader("Accept", containing("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apigeeResponse)));

        String requestUrl = UriComponentsBuilder
                .fromHttpUrl(base.toString())
                .queryParam("priceLabelType", "ShowWasThenNow")
                .toUriString();
        ResponseEntity<ProductList> response = template.getForEntity(requestUrl, ProductList.class);

        // THEN
        ProductList result = response.getBody();

        assertEquals(1, result.size());
        Assert.assertEquals("Was £12.99, now £9.99", result.get(0).priceLabel);
    }

    @Test
    public void showWasThenNowPriceLabelWithThen2PriceWithTenPoundsOrMoreIntegers() throws JsonProcessingException {
        // GIVEN
        ApigeePrice apigeePrice = new ApigeePrice("112.00", "111.00", "110.00", "19.00", "GBP");
        ApigeeProduct apigeeApigeeProduct = new ApigeeProduct("pid1", "title1", Collections.emptyList(), apigeePrice);
        String apigeeResponse = json(new ApigeeProductList(Collections.singletonList(apigeeApigeeProduct)));

        // WHEN
        stubFor(get(urlEqualTo("/reducedPriceProducts"))
                .withHeader("Accept", containing("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apigeeResponse)));

        String requestUrl = UriComponentsBuilder
                .fromHttpUrl(base.toString())
                .queryParam("priceLabelType", "ShowWasThenNow")
                .toUriString();
        ResponseEntity<ProductList> response = template.getForEntity(requestUrl, ProductList.class);

        // THEN
        ProductList result = response.getBody();

        assertEquals(1, result.size());
        Assert.assertEquals("Was £112, then £110, now £19", result.get(0).priceLabel);
    }

    @Test
    public void showPercDiscountPriceLabel() throws JsonProcessingException {
        // GIVEN
        ApigeePrice apigeePrice = new ApigeePrice("12.00", null, null, "9.00", "GBP");
        ApigeeProduct apigeeApigeeProduct = new ApigeeProduct("pid1", "title1", Collections.emptyList(), apigeePrice);
        String apigeeResponse = json(new ApigeeProductList(Collections.singletonList(apigeeApigeeProduct)));

        // WHEN
        stubFor(get(urlEqualTo("/reducedPriceProducts"))
                .withHeader("Accept", containing("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apigeeResponse)));

        String requestUrl = UriComponentsBuilder
                .fromHttpUrl(base.toString())
                .queryParam("priceLabelType", "ShowPercDiscount")
                .toUriString();
        ResponseEntity<ProductList> response = template.getForEntity(requestUrl, ProductList.class);

        // THEN
        ProductList result = response.getBody();

        assertEquals(1, result.size());
        Assert.assertEquals("25% off - now £9.00", result.get(0).priceLabel);
    }

    @Test
    public void showPercDiscountPriceLabelWithTenPoundsOrMoreIntegers() throws JsonProcessingException {
        // GIVEN
        ApigeePrice apigeePrice = new ApigeePrice("120.00", null, null, "90.00", "GBP");
        ApigeeProduct apigeeApigeeProduct = new ApigeeProduct("pid1", "title1", Collections.emptyList(), apigeePrice);
        String apigeeResponse = json(new ApigeeProductList(Collections.singletonList(apigeeApigeeProduct)));

        // WHEN
        stubFor(get(urlEqualTo("/reducedPriceProducts"))
                .withHeader("Accept", containing("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apigeeResponse)));

        String requestUrl = UriComponentsBuilder
                .fromHttpUrl(base.toString())
                .queryParam("priceLabelType", "ShowPercDiscount")
                .toUriString();
        ResponseEntity<ProductList> response = template.getForEntity(requestUrl, ProductList.class);

        // THEN
        ProductList result = response.getBody();

        assertEquals(1, result.size());
        Assert.assertEquals("25% off - now £90", result.get(0).priceLabel);
    }

    @Test
    public void showWasNowPriceLabelIsDefault() throws JsonProcessingException {
        // GIVEN
        ApigeePrice apigeePrice = new ApigeePrice("12.99", null, null,"9.99", "GBP");
        ApigeeProduct apigeeApigeeProduct = new ApigeeProduct("pid1", "title1", Collections.emptyList(), apigeePrice);
        String apigeeResponse = json(new ApigeeProductList(Collections.singletonList(apigeeApigeeProduct)));

        // WHEN
        stubFor(get(urlEqualTo("/reducedPriceProducts")).withHeader("Accept", containing("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apigeeResponse)));

        String requestUrl = UriComponentsBuilder
                .fromHttpUrl(base.toString())
                .toUriString();
        ResponseEntity<ProductList> response = template.getForEntity(requestUrl, ProductList.class);

        // THEN
        ProductList result = response.getBody();

        assertEquals(1, result.size());
        Assert.assertEquals("Was £12.99, now £9.99", result.get(0).priceLabel);
    }

    @Test
    public void shouldReturnOnlyReducedProducts() throws JsonProcessingException {
        // GIVEN
        ApigeePrice apigeePrice1 = new ApigeePrice("2.00", null,null, "1.00", "GBP");
        ApigeePrice apigeePrice2 = new ApigeePrice("", null, null,"3.00", "USD");
        ApigeeProduct apigeeApigeeProduct1 = new ApigeeProduct("pid1", "title1", Collections.emptyList(), apigeePrice1);
        ApigeeProduct apigeeApigeeProduct2 = new ApigeeProduct("pid2", "title2", Collections.emptyList(), apigeePrice2);
        String apigeeResponse = json(new ApigeeProductList(Arrays.asList(apigeeApigeeProduct1, apigeeApigeeProduct2)));

        // WHEN
        stubFor(get(urlEqualTo("/reducedPriceProducts")).withHeader("Accept", containing("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apigeeResponse)));

        String requestUrl = UriComponentsBuilder
                .fromHttpUrl(base.toString())
                .toUriString();
        ResponseEntity<ProductList> response = template.getForEntity(requestUrl, ProductList.class);

        // THEN
        ProductList result = response.getBody();

        assertEquals(1, result.size());
        Assert.assertEquals("pid1", result.get(0).productId);
    }

    @Test
    public void shouldReturnProductsSortedByPriceReductionDescending() throws JsonProcessingException {
        // GIVEN
        int numberOfProducts = 10;
        int priceStep = 3; // Where the Maximum Common Denominator with the numberOfProducts is 1
        List<ApigeeProduct> apigeeProducts = new ArrayList<>(numberOfProducts);
        for (int i=0; i<numberOfProducts; i++) {
            int nowPrice = i * priceStep % numberOfProducts;
            ApigeePrice apigeePrice = new ApigeePrice("102.99", null, null, nowPrice + ".99", "GBP");
            apigeeProducts.add(new ApigeeProduct("pid" + i, "title" + i, Collections.emptyList(), apigeePrice));
        }
        String apigeeResponse = json(new ApigeeProductList(apigeeProducts));

        // WHEN
        stubFor(get(urlEqualTo("/reducedPriceProducts")).withHeader("Accept", containing("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apigeeResponse)));

        String requestUrl = UriComponentsBuilder
                .fromHttpUrl(base.toString())
                .toUriString();
        ResponseEntity<ProductList> response = template.getForEntity(requestUrl, ProductList.class);

        // THEN
        ProductList result = response.getBody();

        assertEquals(result.size(), numberOfProducts);
        for (int i=0; i<numberOfProducts; i++)
            Assert.assertEquals("Element n. " + i + " has unexpected price (wrong reduction order)", "£" + i + ".99", result.get(i).nowPrice);
    }

    @Test
    public void priceIsNotASimpleString() {
        // GIVEN
        String apigeeResponse = getApigeeResponse(getApigeeProductJSON("\"pid\"", "\"title\"", "\"2.00\"", "{\"from\":\"55.00\",\"to\":\"100.00\"}", "\"GBP\""));

        // WHEN
        stubFor(get(urlEqualTo("/reducedPriceProducts")).withHeader("Accept", containing("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apigeeResponse)));

        ResponseEntity<ProductList> response = template.getForEntity(base.toString(),
                ProductList.class);

        // THEN
        ProductList result = response.getBody();
        assertEquals(0, result.size());
    }

    private String getApigeeResponse(String apigeeProduct) {
        return "{ \"products\": [" + apigeeProduct + "], \"otherstuff\": \"othervalue\" }";
    }

    private String getApigeeProductJSON(final String pid, final String title, final String was, final String now, final String currency) {
        return "{" +
                "  \"productId\":" + pid + "," +
                "  \"title\":" + title + "," +
                "  \"extra_field\":\"somevalue\"," +
                "  \"price\":{" +
                "    \"was\":" + was + "," +
                "    \"then1\":\"\"," +
                "    \"then2\":\"\"," +
                "    \"now\":" + now + "," +
                "    \"uom\":\"\"," +
                "    \"currency\":" + currency +
                "  }," +
                "  \"colorSwatches\":[]" +
                "}";
    }
}
